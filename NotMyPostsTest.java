package Stage3;

import Stage3.Logging.AbstractTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class NotMyPostsTest extends AbstractTest {

    @Test
    void getNotMyPostsPositiveTest() {
        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 9)
                .when()
                .get(getBaseUrl() + "?owner=notMe&sort=createdAt&order=ASC&page={page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getNotMyPostsPositive2Test() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 1)
                .when()
                .get(getBaseUrl() + "?owner=notMe&sort=createdAt&order=ASC&page={page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getNotMyPostsNegativeTest() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 0)
                .when()
                .get(getBaseUrl() + "?owner=notMe&sort=createdAt&order=ASC&page={page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getNotMyPostsNegative2Test() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 155678899)
                .when()
                .get(getBaseUrl() + "?owner=notMe&sort=createdAt&order=ASC&page={page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getNotMyPostsNegative3Test() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 76565)
                .when()
                .get(getBaseUrl()+"?owner=notMe&sort=createdAt&order=ASC&page={page}")
                .then()
                .statusCode(200);
    }
}