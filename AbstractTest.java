package Stage3.Logging;

import org.junit.jupiter.api.BeforeAll;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public abstract class AbstractTest {
    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String Token;
    private static String baseUrl;

    @BeforeAll
    static void initTest() throws IOException {
        configFile = new FileInputStream ("src/main/resources/my.properties.properties");
        prop.load(configFile);

        Token =  prop.getProperty("Token");
        baseUrl= prop.getProperty("base_url");
    }

    public static String getToken() {
        return Token;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }
}

