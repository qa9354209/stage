package Stage3.Logging;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class LogTest extends AbstractTest {

    @BeforeAll
    static void setUp(){

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

    }
    @Test
    void getRecipeWithQueryParametersPositiveTest() {
        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .queryParam("Login", "GB202302d8e3c9")
                .queryParam("Password", "5c77f4fe34")
                .when()
                .get("https://test-stand.gb.ru/user/profile")
                .then()
                .statusCode(200);
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++=");

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .queryParam("Login", "GB202302d8e3c9")
                .queryParam("Password", "5c77f4fe34")
                .log().all()
                .when()
                .get("https://test-stand.gb.ru/user/profile");

    }

    @Test
    void getResponseLogTest(){
        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .queryParam("Login", "GB202302d8e3c9")
                .queryParam("Password", "5c77f4fe34")
                .log().all()
                .when()
                .get("https://test-stand.gb.ru/user/profile")
                .prettyPeek();
    }

    @Test
    void getErrorTest(){
        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .queryParam("Login", "GB202302d8e3c9")
                .queryParam("Password", "5c77f4fe34")
                .when()
                .get("https://test-stand.gb.ru/user/profile")
                .then().statusCode(201);
    }
}