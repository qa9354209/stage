package Stage3.MyPostsTest;

import Stage3.Logging.AbstractTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class MyPostsTest extends AbstractTest {

    @Test
    void getMyPostsPositiveTest() {
        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 12476)
                .when()
                .get(getBaseUrl() + "posts/{page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getMyPostsPositive2Test() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 12482)
                .when()
                .get(getBaseUrl() + "posts/{page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getMyPostsNegativeTest() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 0)
                .when()
                .get(getBaseUrl() + "posts/{page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getMyPostsNegative2Test() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 155678899)
                .when()
                .get(getBaseUrl() + "posts/{page}")
                .then()
                .statusCode(200);
    }

    @Test
    void getNotMyPostsNegative3Test() {

        given()
                .queryParam("Token", "f4ab8929e0b2082e06e340562ef3c5a7")
                .pathParam("page", 76565)
                .when()
                .get(getBaseUrl()+"posts/{page}")
                .then()
                .statusCode(200);
    }
}